// slider autoplay
$(document).ready(function () {
  $(".autoplay").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    dots: true,
    prevArrow: $(".btnPrev"),
    nextArrow: $(".btnNext"),
    dotsClass: $("slick-dots"),
  });
});

// movie lists
$(".multiple-items").slick({
  infinite: true,
  slidesToShow: 4,
  slidesToScroll: 4,
});

// popup video
$('[data-fancybox]').fancybox({
  youtube : {
      controls : 0,
      showinfo : 0
  },
  vimeo : {
      color : 'f00'
  }
});
